#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int main(int **argc, char **argv)
{
	float x;
	x=5;
	fprintf(stderr,"value of x = %f\n", x);
	x=6;
	fprintf(stderr,"value of x = %f\n", x);

	x= x+1;
	{
	    float x=2*x;
	    fprintf(stderr,"the inner value of x = %f\n", x);
	}
	fprintf(stderr,"final value of x = %f\n", x);
	return 0;
}
