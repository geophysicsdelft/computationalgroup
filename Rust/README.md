# Rust

The following 2 examples could be a reason to switch to rust.

## Buffer Overflow

```
gcc -g stackBufferOverflow.c 
./a.out Hello
./a.out AAAAAAAAAAAAAAAAAAAA\x08\x35\xC0\x80
gdb a.out 
run AAAAAAAAAAAAAAAAAAAA\x08\x35\xC0\x80
```

Try also 

```
gcc stackBufferOverflow1.c 
./a.out
```

## Basic examples

This directory contain basic examples that show how rust is different from C/Fortran90

To compile with rust load the following modules on DelftBlue
```
module load 2022r2 rust
```

To install rust in your laptop use:
```
curl --proto '=https' --tlsv1.2 https://sh.rustup.rs -sSf | sh
```

### 0. cargo: Rust's package manager

Try the following commands

```
cargo new hello_cargo
cd hello_cargo 
cargo build
```

Try-out
```
cargo run
cargo check
```

to compile it with optimizations.
```
cargo build --release 
```

### 1. hello0.rs .c

Print a string in C and with rust.

Compilation:

```
rustc hello0.rs # => produces executable hello0
gcc hello0.c -o hello0 # => produces executable hello0
```

### 2. Borrow checker

Inspect the program Hello1.c , compile and run:
```
gcc hello1.c -o chello1 
./chello1
```

You can do the same with Hello1.rs 
```
rustc hello1.rs
./hello1
```
Errors?  =>change let x=5; ==> let mut x=5;

### 3. more rust to explore

```
rustc hello2.rs
./hello2
```
compilation will fail to get more details about the error:
```
rustc --explain E0382
```

Also compile and run the C-program:
```
gcc hello2.c -o chello2;
./chello2
```

Try: uncomment the second part in hello2.c and compile run again. 

### 4. Deep copy

```
rustc hello3.rs
./hello3
```

If you’ve heard the terms shallow copy and deep copy while working with other languages, the concept of copying the pointer, length, and capacity without copying the data probably sounds like making a shallow copy. But because Rust also invalidates the first variable, instead of being called a shallow copy, it’s known as a move. In this example, we would say that s1 was moved into s2.


### 5. References 

```
rustc own0.rs
./own0
```
Try to solve the compilation error

### 6. borrowing a value 

```
rustc own1.rs
./own1
```

### 7. concurrency

```
rustc concur1.rs
./concur1
```

